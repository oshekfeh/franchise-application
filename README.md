# Franchise Application

This project is a CRUD exmaple using spring-boot / Angularjs / MySQL.

To run the project you need to:

1 - Put your DB username and password in src/main/resources/application.properties like this:
```
spring.datasource.url=jdbc:mysql://localhost:3306/franchise_application?useSSL=false&serverTimezone=UTC
spring.datasource.username=root #set here your username
spring.datasource.password=12345678 #set here your password
```

2 - Execute the migration file `franchise_application.sql` in the following path src/main/resources/migrations.

3 - Run the project using java IDE, or by running the project jar file using the following commands:
```shell
cd /path/to/the/project/folder/
java -jar target/franchise-application-0.0.1-SNAPSHOT.jar
```

4 - Open the browser at url `http://localhost:8080`.
