package com.bulutmd.franchiseapplication.api.response;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ErrorResponse  implements Serializable {
    public List<Error> errors;

    public ErrorResponse() {
        errors = new ArrayList<>();
    }

    public ErrorResponse(List<Error> errors) {
        this.errors = errors;
    }

    public List<Error> getErrors() {
        return errors;
    }

    @Override
    public String toString() {
        return "ErrorResponse{" +
                "errors=" + errors +
                '}';
    }
}

