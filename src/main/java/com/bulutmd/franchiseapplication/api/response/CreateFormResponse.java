package com.bulutmd.franchiseapplication.api.response;

import lombok.ToString;

import java.io.Serializable;

@ToString
public class CreateFormResponse implements Serializable {
    public FormResponse result;
}
