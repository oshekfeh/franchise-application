package com.bulutmd.franchiseapplication.api.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

@ToString
public class FormResponse implements Serializable {
    public Integer id;

    public String applicantName;

    public String idNumber;

    @JsonFormat(pattern="yyyy-MM-dd")
    public Date birthDate;

    public String address;

    public String phone;

    public String email;

    public String experienceDescription;

    public String interestDescription;

    public Integer plannedInvestmentAmountUsd;

    public Integer operationAreaId;

    public String additionalNote;

    public String formNumber;
}
