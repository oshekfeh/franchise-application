package com.bulutmd.franchiseapplication.api.response;

import lombok.ToString;

import java.io.Serializable;

@ToString
public class GetFormResponse implements Serializable {
    public FormResponse result;
}
