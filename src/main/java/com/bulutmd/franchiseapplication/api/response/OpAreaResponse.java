package com.bulutmd.franchiseapplication.api.response;

import lombok.ToString;

import java.io.Serializable;

@ToString
public class OpAreaResponse implements Serializable {
    public Integer id;

    public String name;
}
