package com.bulutmd.franchiseapplication.api.impl;

import com.bulutmd.franchiseapplication.api.FormApi;
import com.bulutmd.franchiseapplication.api.request.FormRequest;
import com.bulutmd.franchiseapplication.api.response.CreateFormResponse;
import com.bulutmd.franchiseapplication.api.response.GetFormResponse;
import com.bulutmd.franchiseapplication.api.response.GetFormsResponse;
import com.bulutmd.franchiseapplication.service.FormService;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
public class FormApiImpl implements FormApi {
    private final FormService service;

    public FormApiImpl(FormService formService) {
        this.service = formService;
    }

    @Override
    public GetFormsResponse getAll() {
        return service.getAll();
    }

    @Override
    public CreateFormResponse createForm(@Valid FormRequest request) {
        return service.createForm(request);
    }

    @Override
    public GetFormResponse get(Integer formId) {
        return service.get(formId);
    }

    @Override
    public GetFormResponse update(@Valid FormRequest request) {
        return service.update(request);
    }

    @Override
    public void delete(Integer formId) {
        service.delete(formId);
    }
}
