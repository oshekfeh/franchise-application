package com.bulutmd.franchiseapplication.api.impl;

import com.bulutmd.franchiseapplication.api.OpAreaApi;
import com.bulutmd.franchiseapplication.api.response.GetOpAreasResponse;
import com.bulutmd.franchiseapplication.service.OpAreaService;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class OpAreaApiImpl implements OpAreaApi {
    private final OpAreaService service;

    public OpAreaApiImpl(OpAreaService opAreaService) {
        this.service = opAreaService;
    }

    @Override
    public GetOpAreasResponse getAll() {
        return service.getAll();
    }
}
