package com.bulutmd.franchiseapplication.api;

import com.bulutmd.franchiseapplication.api.response.GetOpAreasResponse;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping(path="opArea", produces = MediaType.APPLICATION_JSON_VALUE)
public interface OpAreaApi {
    @GetMapping
    GetOpAreasResponse getAll();
}
