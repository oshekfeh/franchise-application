package com.bulutmd.franchiseapplication.api;

import com.bulutmd.franchiseapplication.api.request.FormRequest;
import com.bulutmd.franchiseapplication.api.response.CreateFormResponse;
import com.bulutmd.franchiseapplication.api.response.GetFormResponse;
import com.bulutmd.franchiseapplication.api.response.GetFormsResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RequestMapping(path="form", produces = MediaType.APPLICATION_JSON_VALUE)
public interface FormApi {
    @GetMapping
    GetFormsResponse getAll();

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    CreateFormResponse createForm(@Valid @RequestBody FormRequest request);

    @GetMapping("/{formId}")
    GetFormResponse get(@PathVariable("formId") Integer formId);

    @PutMapping
    GetFormResponse update(@Valid @RequestBody FormRequest formId);

    @DeleteMapping("/{formId}")
    void delete(@PathVariable("formId") Integer formId);
}
