package com.bulutmd.franchiseapplication.api.handler;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.Collections;
import com.bulutmd.franchiseapplication.api.response.ErrorResponse;

import javax.persistence.EntityNotFoundException;

@RestControllerAdvice
public class EntityNotFoundExceptionHandler {
    @ExceptionHandler(value = EntityNotFoundException.class)
    public ResponseEntity<ErrorResponse> exception(EntityNotFoundException exception) {
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
        return new ResponseEntity<>(new ErrorResponse(
                Collections.singletonList(new Error(
                        exception.getMessage()))),
                headers, HttpStatus.BAD_REQUEST);
    }
}
