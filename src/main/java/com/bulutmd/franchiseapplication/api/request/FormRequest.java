package com.bulutmd.franchiseapplication.api.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.ToString;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.Date;

@ToString
@Data
public class FormRequest implements Serializable {
    private Integer id;

    @NotBlank
    private String applicantName;

    @NotBlank
    private String idNumber;

    @JsonFormat(pattern="yyyy-MM-dd")
    private Date birthDate;

    private String address;

    @NotBlank
    private String phone;

    @Email
    private String email;

    @NotBlank
    private String experienceDescription;

    @NotBlank
    private String interestDescription;

    private Integer plannedInvestmentAmountUsd;

    private Integer operationAreaId;

    private String additionalNote;
}
