package com.bulutmd.franchiseapplication.mapper;

import com.bulutmd.franchiseapplication.api.request.FormRequest;
import com.bulutmd.franchiseapplication.api.response.FormResponse;
import com.bulutmd.franchiseapplication.persistence.entity.Form;
import com.bulutmd.franchiseapplication.persistence.entity.OperationArea;
import com.bulutmd.franchiseapplication.persistence.repository.OperationAreaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Optional;

@Component
public class FormMapper {
    @Autowired
    private OperationAreaRepository operationAreaRepo;

    public Form formRequestToEntity(FormRequest request){
        Form entity = new Form();
        entity.setId(request.getId());
        entity.setApplicantName(request.getApplicantName());
        entity.setIdNumber(request.getIdNumber());
        entity.setBirthDate(request.getBirthDate());
        entity.setAddress(request.getAddress());
        entity.setPhone(request.getPhone());
        entity.setEmail(request.getEmail());
        entity.setExperienceDescription(request.getExperienceDescription());
        entity.setInterestDescription(request.getInterestDescription());
        entity.setPlannedInvestmentAmountUsd(request.getPlannedInvestmentAmountUsd());
        Optional<OperationArea> opArea = operationAreaRepo.findById(request.getOperationAreaId());
        entity.setArea(opArea.get());
        entity.setAdditionalNote(request.getAdditionalNote());
        return entity;
    }

    public FormResponse formEntityToResponse(Form entity){
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyMMdd");

        FormResponse response = new FormResponse();
        response.id = entity.getId();
        response.applicantName = entity.getApplicantName();
        response.idNumber = entity.getIdNumber();
        response.birthDate = entity.getBirthDate();
        response.address = entity.getAddress();
        response.phone = entity.getPhone();
        response.email = entity.getEmail();
        response.experienceDescription = entity.getExperienceDescription();
        response.interestDescription = entity.getInterestDescription();
        response.plannedInvestmentAmountUsd = entity.getPlannedInvestmentAmountUsd();
        response.operationAreaId = entity.getArea() == null ? null: entity.getArea().getId();
        response.additionalNote = entity.getAdditionalNote();
        response.formNumber = dateFormat.format(entity.getCreatedAt()) + String.valueOf(entity.getId());

        return response;
    }
}
