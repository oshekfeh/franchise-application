package com.bulutmd.franchiseapplication.mapper;

import com.bulutmd.franchiseapplication.api.response.OpAreaResponse;
import com.bulutmd.franchiseapplication.persistence.entity.OperationArea;
import com.bulutmd.franchiseapplication.persistence.repository.OperationAreaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class OpAreaMapper {
    @Autowired
    private OperationAreaRepository operationAreaRepo;

    public OpAreaResponse opAreaEntityToResponse(OperationArea entity){
        OpAreaResponse response = new OpAreaResponse();

        response.id = entity.getId();
        response.name = entity.getName();

        return response;
    }
}
