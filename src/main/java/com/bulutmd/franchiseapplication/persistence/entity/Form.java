package com.bulutmd.franchiseapplication.persistence.entity;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "FORMS", indexes = {
        @Index(name = "FROMS_APPLICANT_NAME", columnList = "applicant_name")
        , @Index(name = "FORMS_ID_NUMBER", columnList = "id_number")
        , @Index(name = "FORMS_PHONE", columnList = "phone")
})
@Data
public class Form {
    @Id
    @Column(name = "ID")
    @Access(AccessType.PROPERTY)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @CreationTimestamp
    @Column(name = "CREATED_AT")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    protected Date createdAt = new Date();

    @UpdateTimestamp
    @Column(name = "UPDATED_AT")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    protected Date updatedAt;

    @Column(name = "APPLICANT_NAME", length = 100, nullable = false)
    private String applicantName;

    @Column(name = "ID_NUMBER", length = 45, nullable = false)
    private String idNumber;

    @Column(name = "birth_date")
    private Date birthDate;

    @Column(name = "ADDRESS", length = 45)
    private String address;

    @Column(name = "PHONE", length = 45, nullable = false)
    private String phone;

    @Column(name = "EMAIL", length = 45)
    private String email;

    @Column(name = "EXPERIENCE_DESCRIPTION", length = 1024, nullable = false)
    private String experienceDescription;

    @Column(name = "INTEREST_DESCRIPTION", length = 1024, nullable = false)
    private String interestDescription;

    @Column(name = "PLANNED_INVESTMENT_AMOUNT_USD")
    private Integer plannedInvestmentAmountUsd;

    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.REFRESH, CascadeType.MERGE}, optional = true)
    @JoinColumn(name = "OPERATION_AREAS_ID", referencedColumnName = "ID", foreignKey = @ForeignKey(name = "operation_areas"))
    private OperationArea area;

    @Column(name = "ADDITIONAL_NOTE", length = 1024)
    private String additionalNote;
}
