package com.bulutmd.franchiseapplication.persistence.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "OPERATION_AREAS")
@Data
public class OperationArea {
    @Id
    @Column(name = "ID")
    @Access(AccessType.PROPERTY)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "NAME", length = 45, nullable = false)
    private String name;
}
