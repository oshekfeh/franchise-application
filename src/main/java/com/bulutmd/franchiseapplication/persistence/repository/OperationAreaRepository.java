package com.bulutmd.franchiseapplication.persistence.repository;

import com.bulutmd.franchiseapplication.persistence.entity.OperationArea;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OperationAreaRepository extends JpaRepository<OperationArea, Integer> {
}
