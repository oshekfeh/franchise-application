package com.bulutmd.franchiseapplication.persistence.repository;

import com.bulutmd.franchiseapplication.persistence.entity.Form;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FormRepository extends JpaRepository<Form, Integer> {
}
