package com.bulutmd.franchiseapplication.service;

import com.bulutmd.franchiseapplication.api.response.GetOpAreasResponse;
import com.bulutmd.franchiseapplication.mapper.OpAreaMapper;
import com.bulutmd.franchiseapplication.persistence.entity.OperationArea;
import com.bulutmd.franchiseapplication.persistence.repository.OperationAreaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class OpAreaService {
    @Autowired
    private OperationAreaRepository opAreaRepo;

    @Autowired
    private OpAreaMapper mapper;

    public GetOpAreasResponse getAll(){
        List<OperationArea> areas = opAreaRepo.findAll();

        GetOpAreasResponse response = new GetOpAreasResponse();

        response.result = areas.stream().map(x -> mapper.opAreaEntityToResponse(x))
                .collect(Collectors.toList());

        return response;
    }
}
