package com.bulutmd.franchiseapplication.service;

import com.bulutmd.franchiseapplication.api.request.FormRequest;
import com.bulutmd.franchiseapplication.api.response.CreateFormResponse;
import com.bulutmd.franchiseapplication.api.response.GetFormResponse;
import com.bulutmd.franchiseapplication.api.response.GetFormsResponse;
import com.bulutmd.franchiseapplication.mapper.FormMapper;
import com.bulutmd.franchiseapplication.persistence.entity.Form;
import com.bulutmd.franchiseapplication.persistence.repository.FormRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class FormService {
    @Autowired
    private FormRepository formRepository;

    @Autowired
    private FormMapper formMapper;

    public CreateFormResponse createForm(FormRequest request){
        Form form = formMapper.formRequestToEntity(request);
        formRepository.save(form);

        CreateFormResponse response = new CreateFormResponse();
        response.result = formMapper.formEntityToResponse(form);

        return response;
    }

    public GetFormResponse get(Integer formId){
        Optional<Form> entity = formRepository.findById(formId);

        GetFormResponse response = new GetFormResponse();
        response.result = formMapper.formEntityToResponse(entity.get());

        return response;
    }

    public GetFormsResponse getAll(){
        List<Form> forms = formRepository.findAll();

        GetFormsResponse response = new GetFormsResponse();

        response.result = forms.stream().map(x -> formMapper.formEntityToResponse(x))
                .collect(Collectors.toList());

        return response;
    }

    public GetFormResponse update(FormRequest request){
        if(request.getId() == null || request.getId().equals(0))
            throw new EntityNotFoundException("Form ID can't be null or zero");

        Optional<Form> entity = formRepository.findById(request.getId());

        if(!entity.isPresent())
            throw new EntityNotFoundException("This form is not found!");

        Form form = entity.get();
        form = formMapper.formRequestToEntity(request);
        formRepository.save(form);

        GetFormResponse response = new GetFormResponse();
        response.result = formMapper.formEntityToResponse(entity.get());
        return response;
    }

    public void delete(Integer formId){
        Optional<Form> entity = formRepository.findById(formId);

        if(!entity.isPresent())
            throw new EntityNotFoundException("This form is not found!");

        formRepository.delete(entity.get());
    }
}
