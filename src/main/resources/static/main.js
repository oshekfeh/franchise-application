var app = angular.module("FranchiseApplication", []);

app.controller("ApplicationController", function($scope, $http) {
    $scope.forms = [];
    $scope.aForm = {};
    $scope.areas = [];

    _refreshFormData();

    function _refreshFormData() {
        $http({
            method: 'GET',
            url: '/form'
        }).then(
            function(res) { // success
                $scope.forms = res.data.result;
            },
            function(res) { // error
                console.log("Error: " + res.status + " : " + res.data);
            }
        );
    }

    function _loadAreasData() {
        $http({
            method: 'GET',
            url: '/opArea'
        }).then(
            function(res){
                $scope.areas = res.data.result;
            },
            function(res) {
                console.log("Error: " + res.status + " : " + res.data);
            }
        )
    }

    $scope.submitAForm = function(){
        var method = "";

        if ($scope.aForm.id == null) {
            method = "POST";
        } else {
            method = "PUT";
        }

        $http({
            method: method,
            url: "/form",
            data: angular.toJson($scope.aForm),
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(_success, _error);
    }

    $scope.deleteForm = function(formId){
        $http({
            method: "DELETE",
            url: "/form/" + formId
        }).then(_success, _error);
    }

    $scope.addNewForm = function(){
        $scope.aForm = {};
        _loadAreasData();
    }

    $scope.editForm = function(form){
        $scope.aForm = form;
        $scope.aForm.birthDate = new Date(form.birthDate)
        _loadAreasData();
    }

    function _success(res) {
        _refreshFormData();
    }

    function _error(res) {
        var data = res.data;
        var status = res.status;
        var header = res.header;
        var config = res.config;
        alert("Error: " + status + ":" + data);
    }
});

